# making our app reuseable 
christopherscrumy is a simple django app that can be used to display content  online

add "christopherscrumy" to your install apps in settings like this .
    INSTALLED APPES = [
        ---
        christopherscrumy
    ]

include the christopherscrumy in your project urls.py like this
    path('christopherscrumy/',  include('christopherscrumy.urls'))

Run python manage.py migrate to create the christopherscrumy models

start the development server and visit https://13.58.213.103/admin/ to create a christopherscrumy( you will need the admin app enabled)


