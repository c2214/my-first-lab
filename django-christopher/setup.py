import os

from setuptools import find_packages, setup
'''def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()'''

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
name = 'django-christopher',
version = '0.1',
packages=find_packages(),
include_package_data=True,
License = 'BSD License',
description = 'a simple app to display some content',
long_description = README,
url = "https://13.58.213.103/" ,
author = 'christopher',
author_email = 'chrisushie301@gmail.com' ,


Classifiers = [
    'Environment:: Web Environment',
    'Framework :: Django',
    'intendend Audience :: Developer ',
    'License :: OSI  Approved :: BSD License',
    'operating system :: os independed',
    'programming language :: python',

],
)

'''import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "an_example_pypi_project",
    version = "0.0.4",
    author = "Andrew Carter",
    author_email = "andrewjcarter@gmail.com",
    description = ("An demonstration of how to create, document, and publish "
                                   "to the cheese shop a5 pypi.org."),
    license = "BSD",
    keywords = "example documentation tutorial",
    url = "http://packages.python.org/an_example_pypi_project",
    packages=['an_example_pypi_project', 'tests'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)'''