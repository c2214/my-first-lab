from django.contrib import admin

# Register your models here.
from .models import Connection, ChatMessage

admin.site.register(Connection)
admin.site.register(ChatMessage)