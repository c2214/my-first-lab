from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from.models import ChatMessage, Connection 
import boto3
# Create your views here.
@csrf_exempt 
def test(request):
    return JsonResponse({'message': 'hello Daud'}, status=200)

@csrf_exempt 
def _parse_body(body):
    body_unicode = body.decode('utf-8')
    return json.loads(body_unicode)

@csrf_exempt 
def connect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connected = Connection(connection_id = connection_id)
    connected.save()
    return JsonResponse({'message': 'connect successfully'}, status = 200)
@csrf_exempt 
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId']
    disconnected = Connection.objects.get(connection_id =connection_id)
    disconnected.delete()
    return JsonResponse({'message': 'disconnected successfully'}, status = 200)

@csrf_exempt
def _send_to_connection(connection_id, data):
    gatewayapi=boto3.client('apigatewaymanagementapi', endpoint_url= 'wscat -c wss://1mcheo0qza.execute-api.us-east-2.amazonaws.com/test/', region_name='us-east-2', aws_access_key_id = 'AKIA2T75KSDRPF3SXCEO', aws_secret_access_key = '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',)
    return gatewayapi.post_to_connection(ConnectionId=connection_id,  Data=json.dumps(data).encode('utf-8'))

@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)
    username = body['username']
    content = body['content']
    timestamp = body['timestamp']
    chat = ChatMessage( username = username, message = content, timestamp = timestamp)
    chat.save()
    connections = Connection.objects.all()
    data = {'messages':[body]}
    for connection in connections:
        print(connection)
        _send_to_connection(connection.connection_id, data)
    return JsonResponse({'message': 'message has been sent to all connections successfully'}, status = 200)

@csrf_exempt
def get_recent_messages(request):
    messages = []
    recentchat = ChatMessage.objects.all().order_by("-id")
    for i in range(len(recentchat)):
        data = {
            'username' :recentchat[i].username,
            'message' :recentchat[i].message,
            'timestamp':recentchat[i].timestamp,
        }
        messages.append(data)
    data = {'messages':messages}
    for connection in Connection.objects.all():
        _send_to_connection(connection.connection_id, data)
    return JsonResponse(data, status = 200)
    


