from django.apps import AppConfig


class ChristopherscrumyapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Christopherscrumyapi'
