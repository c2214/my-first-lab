from django.shortcuts import render

# Create your views here.
from .models import ScrumyGoalsApi
from .serializers import ScrumyGoalsApiSerializer
from rest_framework import viewsets

class ScrumyGoalsViewSet(viewsets.ModelViewSet):
    queryset = ScrumyGoalsApi.objects.all().order_by('goal_name')
    serializer_class = ScrumyGoalsApiSerializer