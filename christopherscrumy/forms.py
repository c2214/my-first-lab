from django import forms
from .models import ScrumyGoals, GoalStatus
from django.contrib.auth.models import User

class SignupForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name','email','username','password']


class CreateGoalForm(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name', 'user']

class GoalForm(forms.ModelForm):
    class Meta:
        model = GoalStatus
        fields = ['status_name']


class MyGoalStatus(forms.ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ['goal_name', 'goal_status', 'user']
        widgets = {
            'goal_status': forms.RadioSelect
        }
