from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.models import User,auth
from christopherscrumy.models import GoalStatus
from christopherscrumy.models import ScrumyGoals
from christopherscrumy.models import ScrumyHistory
import random
from django.http import Http404
from .forms import SignupForm, CreateGoalForm, MyGoalStatus
from django.contrib.auth.models import Group




check = random.randint(1000, 9999)
# Create your views here.
def index(request):
    user = User.objects.get(username ='John')
    if request.method =='POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            my_group = Group.objects.get(name = 'Developer')
            my_group.user_set.add(user)
            return redirect('mtext')
            #return HttpResponse(request, 'Thank you dear')
        
    else:
        form = SignupForm()
    #goal= ScrumyGoals.objects.filter(goal_name = 'Learn Django')
    #return HttpResponse('<h1> This is a Scrum Application </h1>')
    return render(request, 'index.html', {'form':form})
def text(request):
    return render(request, 'test.html')




'''
def move_goal(request, pk):
    goal_p = ScrumyGoals.objects.get(goal_id = pk)
    goal = {
        'goal_name':goal_p.goal_name, 'goal_status':goal_p.goal_status
    }
    #form = MyGoalForm()
    if request.method == 'POST':
        goal_status = request.POST['goal_status']
        if request.user.id == goal_p.user.id and goal_status
        form = MyGoalForm(request.POST, instance = goal_p)
        if form.is_valid():
            form.save()
            return render (request, 'myform.html', {'my_message': 'Your goal have been replaced'})
    else:
        goal_g = ScrumyGoals.objects.get(goal_id = pk)
        goal_g.save()
        form = MyGoalForm()
        
        return render(request, 'exeption.html', {'form':'form', 'g_name':goal_g})

  
    

    #return render(request, 'exeption.html', {'referencing':error})
'''

def move_goal(request, pk):
	myg = ScrumyGoals.objects.get(goal_id=pk)

    
	Goal = {
		'goal_name': myg.goal_name,
		'goal_status':myg.goal_status
	}
    
	if request.method == 'POST':
		goal_status = request.POST['goal_status']
		if request.user.id == myg.user.id and goal_status != 4:
			goalStatus = GoalStatus.objects.get(pk=goal_status)
			myg.goal_status = goalStatus
			myg.save()
			return redirect('home')
		elif request.user.groups.filter(name='Admin') or request.user.groups.filter(name='Quality Assurance'):
			goalStatus = GoalStatus.objects.get(pk=goal_status)
			myg.goal_status = goalStatus
			myg.save()
			return redirect('home')
		else:
			context={
			"form": MyGoalStatus(Goal),
			"groups": ', '.join(map(str, request.user.groups.all())),
			'message':'action can not be completed because you either did not create the goal or not an Admin or part of the Qualiy Assurance team'
		}
			return render(request, 'exeption.html', context)
			
	else:
		context={
			"form": MyGoalStatus(Goal),
			"groups": ', '.join(map(str, request.user.groups.all()))
		}
		return render(request, 'exeption.html', context)
def add_goal(request):
    #user = User.objects.get(username = 'Louis')
    #new = GoalStatus.objects.create(status_name = 'Weakly goal' )
    #ouruser = User.objects.create(username = 'Louis')
    #ouruser.save()
    #new.save()
    #check = random.randint(1000, 9999)
    #messages.info(request, check)
    #print(check)
   
    #
    '''user = User.objects.get(username = 'Louis')
    newa = GoalStatus.objects.get(status_name = 'Weekly goal' )
    goalid = random.randint(1000,9999)
    checking = ScrumyGoals.objects.filter(goal_id = goalid).first()
    'if checking == None:
        new_goal = ScrumyGoals.objects.create( goal_status = newa, goal_name = 'Keep Learning Django', goal_id = goalid, created_by = 'Louis', moved_by = 'Louis', owner = 'Louis',  user = user)
        #new_goal.goal_status.save()
        new_goal.save()
        return HttpResponse(new_goal.goal_name)
    else:
        return redirect('addgoal')'''
    if request.method =='POST':
        form = CreateGoalForm(request.POST)
        if form.is_valid():
            form.save()
        
    else:
        form = CreateGoalForm()
    #goal= ScrumyGoals.objects.filter(goal_name = 'Learn Django')
    #return HttpResponse('<h1> This is a Scrum Application </h1>')
    return render(request, 'addgoal.html', {'form':form})



    

def home(request):
    user = User.objects.all()
    week_goal = GoalStatus.objects.get(status_name ='Weekly goal').my_status.all()
    #week_goal = wee_goal.scrumygoals_set.all()
    Day_goal = GoalStatus.objects.get(status_name = 'Daily goal').my_status.all()
    #Day_goal = Da_goal.scrumygoals_set.all()
    Very_goal = GoalStatus.objects.get(status_name ='Verify goal').my_status.all()
    #Very_goal = Ver_goal.scrumygoals_set.all()goal
    Done_goal = GoalStatus.objects.get(status_name = 'Done goal').my_status.all()
    #Done_goal = Don_goal.scrumygoals_set.all()

   # user = User.objects.get(username = 'Louis')
    #my_goal_list= ScrumyGoals.objects.filter(goal_name = 'Keep Learning Django')
    #manu = ScrumyGoals.objects.all()
    
    #output = ', '.join([e.goal_name for e in my_goal_list])
    #x = eachgoal.goal_name
    #my_instance = ScrumyGoals.objects.filter(goal_name = 'Learn Django')
    #dictionary = {'context': my_instance}
    context = { 'user': user, 'week_goal': week_goal, 'Day_goal': Day_goal, 'Very_goal': Very_goal, 'Done_goal': Done_goal }
    return render(request, 'home.html', context)
      
def signin(request):
    if request.method =='POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username = username, password = password)

        if user is not None:
            auth.login(request, user)
            return redirect('/home')
        else:
            messages.info(request, 'invalid login credentials')
            return redirect('signin')
    return render(request, 'signin.html')