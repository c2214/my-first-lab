from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


# Create your models here.

class GoalStatus(models.Model):
    status_name = models.CharField(max_length = 1000)
    def __str__(self):
        return self.status_name
class ScrumyGoals(models.Model):
    goal_name = models.CharField(max_length = 1000, default = "")
    goal_id = models.IntegerField(null = True)
    created_by = models.CharField(max_length = 1000, default = "" )
    moved_by = models.CharField(max_length = 1000, default = "")
    goal_status = models.ForeignKey(GoalStatus, on_delete = models.CASCADE, related_name = "my_status", default = "", null = True)
    owner = models.CharField(max_length =1000, default = '')
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name = 'myname', default = "")
    def __str__(self):
        return self.goal_name




class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length = 1000)
    created_by =  models.CharField(max_length = 1000)
    moved_from = models.CharField(max_length = 1000)
    moved_to = models.CharField(max_length = 1000)
    time_of_action  = models.DateTimeField(default = datetime.now)
    goal = models.ForeignKey(ScrumyGoals, on_delete = models.CASCADE)
    def __str__(self):
        return self.created_by
