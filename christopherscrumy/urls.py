from django.urls import path, include
from . import views

urlpatterns = [
    path('index', views.index, name ='index'),
    path('signin', views.signin, name = 'signin'),
    path('home', views.home, name = 'home'),
    path('addgoal', views.add_goal, name = 'addgoal'),
    path('movegoal/<int:pk>/', views.move_goal, name = 'movegoal'),
    #path('accounts/', include('django.contrib.auth.urls')),
    #path('mtext', views.text, name = 'mtext'),
    
]